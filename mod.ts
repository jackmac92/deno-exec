const _parseCmdResponse = async (p: Deno.Command) => {
  const { code, stdout, stderr } = await p.output();

  const decoder = new TextDecoder();

  if (code !== 0) {
    throw new Error(decoder.decode(stderr));
  }

  const out = decoder.decode(stdout);
  return out.split("\n").reverse().slice(1).reverse().join("\n");
};

const _cmdResponseBase = (
  shell: string,
  cmd: string[],
  options: Deno.CommandOptions = {},
): Promise<string> => {
  const p = new Deno.Command(shell, {
    ...options,
    args: ["-c", cmd.join(" ")],
    stdout: "piped",
    stderr: "piped",
  });
  return _parseCmdResponse(p);
};

export const cmdResponse = (
  cmd: string[],
  options: Deno.CommandOptions = {},
): Promise<string> => _cmdResponseBase("bash", cmd, options);

export const cmdResponseZshLoginShell = (
  cmd: string[],
  options: Deno.CommandOptions = {},
): Promise<string> =>
  _cmdResponseBase("zsh", ["-lc", `${cmd.join(" ")}`], options);

/**
 * Escapes a string for safe use as a shell argument.
 * @param str The string to escape.
 * @returns The escaped string.
 */
function escapeShellArg(str: string): string {
  // Escape single quotes and wrap the argument in single quotes
  return `'${str.replace(/'/g, "'\\''")}'`;
}

/**
 * Creates a shell command with escaped arguments.
 * @param template A template string with placeholders for arguments.
 * @param args The arguments to be inserted into the template.
 * @returns The fully constructed command string.
 */
export function createShellCommand(
  template: TemplateStringsArray,
  ...args: string[]
): string {
  let command = template[0];

  // Iterate over the arguments and their positions in the template
  args.forEach((arg, index) => {
    command += escapeShellArg(arg) + template[index + 1];
  });

  return command;
}

const _runCmdInPopupShell = (
  cmd: string,
  options: Deno.CommandOptions = {},
): Promise<Deno.CommandOutput> => {
  const x = new Deno.Command("st", {
    ...options,
    args: [
      "-e",
      "zsh",
      "-lic",
      "direnv",
      "exec",
      "~",
      `${cmd} || { echo "Command '${cmd}' failed..."; zsh }`,
    ],
  });
  return x.output();
};

export const runCmdInPopupShell = async (
  cmd: string,
  options: Deno.CommandOptions = {},
): Promise<Uint8Array> => {
  const { stdout } = await _runCmdInPopupShell(cmd, options);
  return stdout;
};

export const mustRunCmdInPopupShell = async (
  cmd: string,
  options: Deno.CommandOptions = {},
): Promise<Uint8Array> => {
  const { code, stdout, stderr } = await _runCmdInPopupShell(cmd, options);
  if (code > 0) {
    throw new Error(`Command failure for '${cmd}':\n${stderr}`);
  }
  return stdout;
};
