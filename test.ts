import { assertEquals } from "https://deno.land/std/testing/asserts.ts";
import { cmdResponse } from "./mod.ts";

Deno.test("Test get response handler", async () => {
  const resp = await cmdResponse(["echo hi"]);
  assertEquals(typeof resp, "string");
  assertEquals(resp, "hi");
});

Deno.test("Test cmdResponse with gui", { ignore: true }, async () => {
  const resp = await cmdResponse(["alacritty -e s meta select-script"]);
  assertEquals(typeof resp, "string");
});
